<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('design', ['uses' => 'MainController@design']);
Route::get('/', ['uses' => 'MainController@index']);


Route::get('games/snakes-and-ladders', ['uses' => 'GamesController@snakes']);
Route::get('games/blackjack', ['uses' => 'GamesController@blackjack']);




