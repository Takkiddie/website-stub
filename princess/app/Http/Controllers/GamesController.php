<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GamesController extends Controller
{
    function snakes()
    {
        return view('games.snakes');
    }
    function blackjack()
    {
        return view('games.blackjack');
    }
}
