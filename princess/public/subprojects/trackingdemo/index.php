<?php ?>

<!--It would be better to store the code locally, but to keep this page small, I'm importing it.-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    .container {
        margin-top: 20px;
    }
</style>

<div class="container">
    <div class='form_block'>
        <form class='' action='/'>
            <div class="form-group">
                <label for="string_input">String</label>
                <input class="form-control" type="text" required name="string_input" id="string_input" minlength="6"
                       maxlength="20">
            </div>
            <button class="btn btn-primary">Submit</button>
            <a href="/" class="btn btn-primary alphabetize_button">Alphabetize</a>
        </form>
        <ul id="string_list"></ul>
    </div>

    <div class='form_block'>
        <form class='' action='/'>
            <div class="form-group">
                <label for="date_input">Date</label>
                <input class="form-control date_input" type="text" required name="date_input" id="date_input">
            </div>
            <button class="btn btn-primary">Submit</button>
        </form>
        <ul id="date_list"></ul>
    </div>

    <div class='form_block'>
        <form class='' action='/'>
            <div class="form-group">
                <label for="int_input">Integer</label>
                <input class="form-control int_input" required type="text" name="int_input" id="int_input">
            </div>
            <button class="btn btn-primary">Submit</button>
            <a href="/" class="btn btn-primary min_to_max_button">Min To Max</a>
            <a href="/" class="btn btn-primary primes_button">Primes</a>
        </form>
        <ul id="int_list"></ul>
    </div>

    <div class='form_block'>
        <form class='' action='/'>
            <div class="form-group">
                <label for="currency_input">Currency</label>
                <input class="form-control currency_input" required type="text" name="currency_input" id="int_input">
            </div>
            <button class="btn btn-primary">Submit</button>
            <a href="/" class="btn btn-primary min_to_max_button">Min To Max</a>
        </form>
        <ul id="currency_list"></ul>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function () {
        var value_lists = {};

        $("#date_input").datepicker();

        //Validation
        $(".int_input").on('input', function (e) {
            var value = $(this).val();
            var altered_input = value.replace(/[^\d]/g, '');
            $(this).val(altered_input);
        });

        $(".currency_input").on('input', function (e) {
            var value = $(this).val();
            var altered_input = value.replace(/[^\d.-]/g, '');
            $(this).val(altered_input);
        });

        $(".date_input").on('input', function (e) {
            var value = $(this).val();
            var timestamp = Date.parse(value);

            //If not a valid time, or not in a date format.
            //A lot of single integers can be parsed as dates, strangely
            if (isNaN(timestamp) || !value.includes('/')) {
                $(this).val('');
            }
        });


        //Updating format after user has clicked away from field
        $(".currency_input").on('change', function (e) {
            var value = $(this).val();
            var altered_input = value.replace(/[^\d.-]/g, '');
            var number_input = Number.parseFloat(altered_input);

            if (!isNaN(number_input)) {
                altered_input = number_input.toFixed(2);
            }
            $(this).val(altered_input);
        });

        //Controls
        $('.alphabetize_button').on('click', function (e) {
            e.preventDefault();

            var form = $(this).parent();
            var list = form.parent().find('ul');
            var input = form.find('input');

            var input_string = input.val();
            var sorted = input_string.split('').sort().join('');
            input.val(sorted);
        });

        //Will also hear when the user presses enter in none of the forms.
        $('form').on('submit', function (e) {
            e.preventDefault();
            var inputData = $(this).serializeArray();
            var list = $(this).parent().find('ul');
            var ul_id = list.attr('id');

            //Will only get the first, but each form has only one input, so it should work.
            $(this).find('input').val('');

            //Gets only item in list, without needing the key.
            for (var key in inputData) {
                var entry = inputData[key].value;
                //Define a list if there is not one.
                if (typeof(value_lists[ul_id]) == 'undefined') {
                    value_lists[ul_id] = [];
                }
                value_lists[ul_id].push(entry);
                renderSavedList(ul_id);
            }
        });

        $('.min_to_max_button').on('click', function (e) {
            e.preventDefault();

            var form = $(this).parent();
            var list = form.parent().find('ul');
            var ul_id = list.attr('id');

            renderSavedList(ul_id, 'min_to_max');
        });

        $('.primes_button').on('click', function (e) {
            e.preventDefault();

            var form = $(this).parent();
            var list = form.parent().find('ul');
            var input_value = parseInt(form.find('input').val());
            var ul_id = list.attr('id');
            var primes = get_primes(input_value, 5);

            renderList(ul_id, primes);

            console.log(primes);

        });

        //Helpers
        function renderSavedList(id, sortBy = '') {
            var list = value_lists[id];

            //If list is not yet set, don't try to display anything.
            if (typeof(list) === "undefined") {
                return;
            }

            if (sortBy == 'min_to_max') {
                list = list.sort();
            }

            renderList(id, list);
        }

        function isPrime(num) {
            for (var i = 2; i < num; i++) {
                if (num % i === 0) {
                    return false;
                }
            }
            return true;
        }

        function get_primes(start, number_of_primes) {
            var arr = [];

            if(start <= 2)
            {
                arr.push(2);
                start = 3;
            }

            for (var i = start; arr.length < number_of_primes; i += 2) {
                if (isPrime(i)) {
                    arr.push(i);
                }
            }
            return arr;
        }

        function renderList(id, list) {
            $('#' + id).html('');
            for (var key in list) {
                var li = '<li>' + (list[key]) + '</li>';
                $('#' + id).append(li);
            }
        }
    });
</script>

