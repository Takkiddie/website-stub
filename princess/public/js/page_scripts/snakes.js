/**
 * Created by XZoah on 8/9/17.
 */

var number_of_players = 2;  //Total Number of players.
var current_player = 1;     //Who's turn is it?
var players = [];           //An array containing player objects.
var winCheck = 0;           //Checks if a player has won.
var snakeSound = new Audio('/mp3s/snake.mp3');      //Sound for when a player hits a ladder.
var ladderSound = new Audio('/mp3s/ladder.wav');    //Sound for when a player hits a snake.
var stepSound = new Audio('/mp3s/step.mp3');    //Sound for when a player hits a snake.

var snakesAndLadders = [];   //Contains a list start points and destinations for snakes and ladders.

//Space Landed on   =   Space Going to.
snakesAndLadders[3] = 51;
snakesAndLadders[6] = 27;
snakesAndLadders[20] = 70;
snakesAndLadders[36] = 55;
snakesAndLadders[63] = 95;
snakesAndLadders[68] = 98;

//Snakes
snakesAndLadders[25] = 5;
snakesAndLadders[34] = 1;
snakesAndLadders[47] = 19;
snakesAndLadders[65] = 52;
snakesAndLadders[87] = 57;
snakesAndLadders[91] = 61;
snakesAndLadders[99] = 69;

for (var i = 1; i <= number_of_players; i++) {
    var player = {position: 1,index: i};
    players[i] = player;
}

function renderPlayers() {
    $('.snake_space').html('');
    for(var player_key in players){
        var player = players[player_key];
        $('#'+player.position).append("PLAYER "+ player_key + "<br>");
    }
}

function showMove(move)
{
    $('.move').off();
    $('.move').on('click',function(e){
        e.preventDefault();
    });

    if(move > 0)
    {
        setTimeout(function(){
            players[current_player].position = players[current_player].position + 1;
            renderPlayers();
            showMove(move-1);
        }, 100);
    }
    else
    {
        setTimeout(function(){
            doneMoving();
        }, 100);
    }
}

function doneMoving() {
    checkAndRelocate(current_player);
    current_player = current_player + 1;
    if (current_player > number_of_players) {
        current_player = 1;
    }
    //Ready for checking win.
    winCheck = checkWin();

    if(winCheck){
        setTimeout(function() {
            alert('Player '+winCheck+' Has Won.');
        }, 0)
    }

    $('.move').off();
    $('.move').on('click',function(e){
        e.preventDefault();
        makeMove();
    });
}

function checkWin()
{
    var ret = 0;
    for(var player_key in players){
        var player = players[player_key];


        if(player.position >= 100) {
            players[player_key].position = 100;
            renderPlayers();
            ret = player_key;
        }
    }
    return ret;
}

//If landing on a snake or ladder. Relocate.
function checkAndRelocate(current_player) {

    console.log(players[current_player]);

    var pos = players[current_player].position;
    if(snakesAndLadders[pos] !== undefined) {
        players[current_player].position = snakesAndLadders[pos];
        //
        if(snakesAndLadders[pos] < pos) {
            snakeSound.play();
        }
        else {
            ladderSound.play();
        }
    }
    renderPlayers();
}

function makeMove()
{
    if(winCheck == 0) {
        var move = Math.floor((Math.random() * 6) + 1);
        $('#showMove').html(move);
        stepSound.play();
        showMove(move);
    }
}

$('.move').off();
$('.move').on('click',function(e){
    e.preventDefault();
    makeMove();
});

renderPlayers();
