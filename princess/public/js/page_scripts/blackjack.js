const DEALER_NAME = 'Dealer';
const PLAYER_NAME = 'Player ';
const STARTING_CARD_NUMBER = 2;
const MAX_SCORE = 21;
const KING = 13;
const QUEEN = 12;
const JACK = 11;
const ACE = 1;
const SPADES = 'spades';
const DIAMONDS = 'diamonds';
const HEARTS = 'hearts';
const CLUBS = 'clubs';
const SUITS = [SPADES, DIAMONDS, HEARTS, CLUBS];

//This should be a constant
//But javascript does not allow array keys to be set on initialization
var FACE_CARD_NAMES = [];
FACE_CARD_NAMES[KING] = 'King';
FACE_CARD_NAMES[QUEEN] = 'Queen';
FACE_CARD_NAMES[JACK] = 'Jack';
FACE_CARD_NAMES[ACE] = 'Ace';


$(document).ready(function () {
    var player_info = $('#player_info');
    var hit_selector = $('#hit_button');
    var hold_selector = $('#hold_button');
    var player_label = $('#player_move');
    var deck = initializeDeck();
    var number_of_players = 1;
    var players = initializePlayers(number_of_players);
    var player_keys = arrayKeys(players);
    var current_player_key;
    var winnerText = '';
    var gameisOver = false;

    initializePlayerHands();
    doPlayerTurn();

    function initializePlayerHands()
    {
        for(var key in players)
        {
            for(var index = 0; index < STARTING_CARD_NUMBER; index++)
            {
                players[key].cards.push(deck.pop());
            }
        }
    }

    function doPlayerTurn()
    {
        setCurrentPlayer();
        showGameState();

        if(isGameOver(players))
        {
            gameOver((players));
            return;
        }

        //Player has not yet played this turn.
        players[current_player_key].played = false;

        //If AI does anything, do not continue processing.
        if(automaticActions())
        {
            return;
        }

        //Reassign listeners for buttons
        hit_selector.off();
        hit_selector.on('click', hitLink);

        hold_selector.off();
        hold_selector.on('click', function(e){
            e.preventDefault();
            doPlayerTurn();
        });
    }

    //Sets the current player index,
    //Gets ready to set the next one.
    function setCurrentPlayer()
    {
        current_player_key = player_keys.shift();
        player_keys.push(current_player_key);
    }

    function showGameState()
    {
        current_player = players[current_player_key];

        if(gameisOver)
        {
            //Lock up the game.
            player_label.html(winnerText);
            hit_selector.hide();
            hold_selector.hide();
        } else {
            player_label.html('Current player: '+current_player.name);
        }
        var game_state_string = '';

        for(var key in players)
        {
            var player = players[key];
            game_state_string = game_state_string + ('<h3>' + player.name + '</h3>');
            game_state_string = game_state_string + (hand_to_string(player.cards));
            game_state_string = game_state_string + '<br>';
            game_state_string = game_state_string + 'score: ' + scorePlayerCards(player);
        }
        player_info.html(game_state_string);
    }

    function automaticActions()
    {
        //Assume 'hold' if player has 'busted'
        if(scorePlayerCards(players[current_player_key]) > MAX_SCORE)
        {
            doPlayerTurn();
            return true;
        }

        if(players[current_player_key].name === DEALER_NAME)
        {
            doDealerTurn();
            return true;
        }
    }
    function doDealerTurn()
    {
        while
            (
        getWinner(players).name !== DEALER_NAME &&
        !(scorePlayerCards(players[current_player_key]) > MAX_SCORE)
            )
        {
            hit();
        }
        //Dealer Turn;
        doPlayerTurn();
    }

    function isGameOver(players)
    {
        //If no one has played for a full round, game over.
        var anyone_has_played = false;
        for(var key in players)
        {
            anyone_has_played = anyone_has_played || players[key].played;
        }
        return !anyone_has_played;
    }

    function gameOver(players)
    {
        var winner = getWinner(players);
        gameisOver = true;

        if(winner !== null)
        {
            winnerText = winner.name + " Wins!";
        } else {
            winnerText = "Game Tied!";
        }

        showGameState();

        return;
    }

    function hit()
    {
        players[current_player_key].played = true;
        players[current_player_key].cards.push(deck.pop());
        showGameState();
    }

    function hitLink(e)
    {
        e.preventDefault();
        hit();
        //Assume 'hold' if player has 'busted'
        if (scorePlayerCards(players[current_player_key]) > MAX_SCORE) {
            doPlayerTurn();
        }
    }
});

function getWinner(players)
{
    //Dealers win ties.
    //Will need additional calculations for more than one player.

    var winner = null;
    var winnerScore = 0;
    for(var key in players)
    {
        let playerScore = scorePlayerCards(players[key]);
        if
        (
            playerScore <= MAX_SCORE &&
            playerScore > winnerScore
        )
        {
            winner = players[key];
            winnerScore = playerScore;
        }
    }

    return winner;
}

function scorePlayerCards(player)
{
    var aces = 0;
    var cards = player.cards;
    var score = 0;
    for(var key in cards)
    {
        var card = cards[key];
        if(card.value != ACE)
        {
            score = score+ card.value;
        } else {
            aces++;
        }
    }
    while(aces > 0)
    {
        //Do not factor the 'current' ace into calculations
        aces = aces - 1;

        //If the score is less than 21
        //Assuming all other aces are valued at 1
        if(score+ACE > MAX_SCORE-aces)
        {
            score = score+ACE;
        } else {
            score = score+1;
        }
    }
    return score;
}

function createPlayer(player_name)
{
    return {cards: [], name: player_name, played: true}
}

function initializePlayers(number_of_players)
{
    var players = [];
    players.push(createPlayer(DEALER_NAME));
    //Will only itterate once for now.
    for(var player_index = 1; player_index <= number_of_players; player_index++)
    {
        players.push(createPlayer(PLAYER_NAME+String(player_index)));
    }
    return players;
}

function initializeDeck() {
    var deck = [];

    for (var key in SUITS) {
        var suit = SUITS[key];
        for (var index = ACE; index <= KING; index++) {
            deck.push(makeCard(index, suit));
        }
    }
    return shuffle(deck);
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function makeCard(value, suit) {
    var card_name = String(value);
    var card_value = value;

    if (typeof(FACE_CARD_NAMES[value]) !== 'undefined') {
        card_name = FACE_CARD_NAMES[value];

        if (value == ACE) {
            card_value = 11;
        } else {
            card_value = 10;
        }
    }

    return {name: card_name, value: card_value, suit: suit};
}

function hand_to_string(hand)
{
    var cards_string = '';

    for(var key in hand)
    {
        var card = hand[key];
        var card_string = card.name + ' of ' + card.suit;

        cards_string = cards_string + (card_string);
        cards_string = cards_string + '<br>';
    }
    return cards_string;
}

function arrayKeys(arr)
{
    var ret = [];
    for(var key in arr)
    {
        ret.unshift(key);
    }
    return ret;
}

