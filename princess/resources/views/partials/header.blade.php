
<div class="header navigation">
    <div class ="container">
        <ul>
            <li><a href='/'>Home</a></li>
            <li>|</li>
            <li>
                <div class="btn-group">
                    <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        Games
                    </a>
                    <div class="dropdown-menu colorScheme">
                        <ul>
                            <a href='/games/snakes-and-ladders'>
                                <li>
                                    Snakes and Ladders
                                </li>
                            </a>
                            <a href='/games/blackjack'>
                                <li>
                                    blackjack
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </li>
            <li>|</li>
            <li>
                <div class="btn-group">
                    <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Other Links
                    </a>
                    <div class="dropdown-menu colorScheme">
                        <ul>
                            <a href='/'>
                                <li>
                                    One
                                </li>
                            </a>
                            <a href='/'>
                                <li>
                                    Two
                                </li>
                            </a>
                            <a href='/'>
                                <li>
                                    Three
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>

    </div>
</div>
