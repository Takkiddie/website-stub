@extends('templates.games')

@section('content')

    <h1>Snakes and Ladders</h1>

    <a class='move' href='/'>Make A move!</a>

    <!-- Credit to http://ww3.iransafebox.net/free-printable-board-games-snakes-and-ladders/ for board design. -->
    <table id='snake_board'>
        <?php
        for ($row = 9 ; $row >= 0; $row--)
        {
            echo '<tr>';
            for ($column = 1 ; $column <= 10; $column++)
            {
                $cell_text = $column+($row*10);
                echo '<td style=""><span class="snake_space" id="'.$cell_text.'">'.$cell_text.'</span></td>';
            }
            echo '</tr>';
        }
        ?>
    </table>

    <br>
    <a class='move' href='/'>Make A move!</a>

    <div id='showMove'></div>

@endsection

@section('scripts')

    <script src="/js/page_scripts/snakes.js"></script>

@endsection