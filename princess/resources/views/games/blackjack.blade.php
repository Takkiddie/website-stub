@extends('templates.games')

@section('content')



@endsection

@section('scripts')

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <br>
    <div class="container">
        <div id="game_container">
            <h1>Blackjack</h1>
            <h2><span id='player_move'></span></h2>
            <a id='hit_button' href = '/'>Hit</a>
            <a id='hold_button'  href = '/'>Hold</a>
            <br>
            <div id='player_info'>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="/js/page_scripts/blackjack.js"></script>

@endsection