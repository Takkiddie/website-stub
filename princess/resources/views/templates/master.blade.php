<div class="contains_all colorScheme">

    <?php ?>
    <link rel="stylesheet" href="/css/template/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">

    @include('partials.header')

    <div class="container">
        <div class="colorScheme main content panel panel-default col-xs-7">
            @yield('content')
        </div>
        <div class="colorScheme sidebar panel panel-default col-xs-4">
            <h3>Things to do</h3>
            <p>
                <a href='/games/snakes-and-ladders'>
                    Snakes and Ladders
                </a>
            </p>
            <p>
                <a href='/games/blackjack'>
                    blackjack
                </a>
            </p>
        </div>
    </div>

    <script type="text/javascript" src="js/template/jquery.min.js"></script>
    <script type="text/javascript" src="js/template/bootstrap.min.js"></script>

    @yield('scripts')


    @include('partials.footer')

</div>
