# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is for a small laravel-based website that contains various projects by Ian Scarborough.

It was originally going to contain a website 'advocated' for silent protagonists.
The website was to be named 'princesses who have been excused' in reference to the meme from the legend of zelda animated series.
Thus the name of the root folder.

### How do I get set up? ###

Clone the repository
Install Composer
Run NPM install
Configure the Hoasts file to point at the /princess/public/index.php
